export class AddAdminUser {
    static readonly type = '[any] Add'

    constructor(public payload: any) {}
}

export class RemoveAdminUser {
    static readonly type = '[any] Remove'

    constructor(public payload: any) {}
}