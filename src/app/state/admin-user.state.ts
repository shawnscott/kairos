import { State, Action, StateContext, Selector } from '@ngxs/store';
import { AddAdminUser, RemoveAdminUser } from './../actions/admin-user.actions';

export class AdminUserStateModel {
    user;
}

@State<AdminUserStateModel>({
    name: 'adminUsers'
})
export class AdminUserState {

    @Selector()
    static getAdminUser(state: AdminUserStateModel) {
        return state.user;
    }

    @Action(AddAdminUser)
    add({ getState, patchState }: StateContext<AdminUserStateModel>, { payload }: AddAdminUser) {
        const state = getState();
        patchState({
            user: payload
        })
    }

    @Action(RemoveAdminUser)
    Remove({ getState, patchState }: StateContext<AdminUserStateModel>, { payload }: RemoveAdminUser) {
        const state = getState();
        patchState({
            user: ''
        })
    }

}

