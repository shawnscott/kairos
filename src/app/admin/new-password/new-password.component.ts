import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AmplifyService } from 'aws-amplify-angular';
import { Auth } from 'aws-amplify';
import { Store, Select } from '@ngxs/store';
import { AddAdminUser } from './../../actions/admin-user.actions';
import { Observable } from 'rxjs/Observable';

import { AdminUserState, AdminUserStateModel } from './../../state/admin-user.state';

@Component({
  selector: 'app-new-password',
  templateUrl: './new-password.component.html',
  styleUrls: ['./new-password.component.css']
})
export class NewPasswordComponent implements OnInit {

  @Select(AdminUserState.getAdminUser) adminUser$: Observable<any>
  adminUser: any;
  user: any;
  signedIn: boolean;
  greeting: string;
  invalidPassword: boolean = false;
  invalidFormat: boolean = false;
  state = {
    email: '',
    password: '',
    passwordConfirm: '',
    errors: {
      cognito: null,
      blankfield: false
    }
  }

  constructor( private amplifyService: AmplifyService, private router: Router, private store: Store ) {}

  ngOnInit() {
    this.adminUser$.subscribe({
      next: user => this.adminUser = user,
      error: err => console.log("error", err)
    })
    if (!this.adminUser) this.router.navigate(['/admin']);
    this.state.email = localStorage.getItem('adminUserEmail').slice(1, this.state.email.length - 1);
  }

  addAdminUser = async (user: any) => {
    this.store.dispatch(new AddAdminUser(user));
  }

  setState = (key: string, val: any) => {
    this.state[key] = val;
  }

  handleChange = evt => {
    let isPassword = evt.target.dataset.pass;
    let val = evt.srcElement.value;
    if (isPassword === 'pass') {
      this.setState('password', val);
    } else if (isPassword === 'pass2') {
      this.setState('passwordConfirm', val)
    } else {
      this.setState('email', val);
    }
  }

  handleSubmit = async evt => {
    evt.preventDefault();
    this.invalidPassword = false;
    this.invalidPassword = false;

    if (this.state.password === this.state.passwordConfirm) {
      const { email, password } = this.state;

      try {
        const passwordChangeResponse = await Auth.completeNewPassword(this.adminUser, password, { email });
        this.addAdminUser(passwordChangeResponse);
        this.router.navigate(['/admin/console']);
      } catch(error) {
        console.log("error", error);
        this.invalidFormat = true;
      }
    } else {
      this.invalidPassword = true;
    }
  }

}
