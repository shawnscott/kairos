import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SigninComponent } from './signin/signin.component';
import { NewPasswordComponent } from './new-password/new-password.component';
import { AdminConsoleComponent } from './admin-console/admin-console.component';

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild([
      { path: 'admin', component: SigninComponent },
      { path: 'admin/newPassword', component: NewPasswordComponent },
      { path: 'admin/console', component: AdminConsoleComponent }
    ])
  ],
  exports: [ RouterModule ]
})
export class AdminRoutingModule { }
