// Testing signin info
// shawn.scott.xd@gmail.com
// Testing1234$$$

import { Component, OnInit, EventEmitter, Output } from '@angular/core'; 
import { AmplifyService } from 'aws-amplify-angular';
import { Auth } from 'aws-amplify';
import { Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AddAdminUser } from './../../actions/admin-user.actions';
 
@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

  signedIn: boolean;
  greeting: string;
  state = {
    email: '',
    password: '',
    errors: {
      cognito: null,
      blankfield: false
    }
  }
  user;
  @Output() addAdminUser$: EventEmitter<any> = new EventEmitter();
    
  constructor( private amplifyService: AmplifyService, private router: Router, private store: Store ) {}

  ngOnInit() {
    this.amplifyService.authStateChange$
      .subscribe(authState => {
        this.signedIn = authState.state === 'signedIn';
        if (!authState.user) {
          this.user = null;
        } else {
          this.user = authState.user;
          this.greeting = "Hello " + this.user.username;
        }
      });
  }

  addAdminUser = async (user: any) => {
    this.store.dispatch(new AddAdminUser(user));
  }

  clearState = () => {
    this.state = {
      email: '',
      password: '',
      errors: {
        cognito: null,
        blankfield: false
      }
    }
  }

  setState = (key: string, val: any) => {
    this.state[key] = val;
  }

  setLocalStorage = (key: string, val: any) => {
    const strVal = JSON.stringify(val);
    localStorage.setItem(key, strVal);
  }

  handleChange = evt => {
    let isPassword = evt.target.dataset.pass;
    let val = evt.srcElement.value;
    if (isPassword) {
      this.setState('password', val);
    } else {
      this.setState('email', val);
    }
  }

  handleSubmit = async evt => {
    evt.preventDefault();

    const { email, password } = this.state;
    const username = email;
    
    try {
      const signInResponse = await Auth.signIn({
        username,
        password
      });
      if (signInResponse.challengeName === "NEW_PASSWORD_REQUIRED") {
        this.setLocalStorage("adminUserEmail", email);
  
        this.addAdminUser(signInResponse);
        this.router.navigate(['/admin/newPassword']);
      } else {
        this.router.navigate(['/admin/console']);
      }
      this.clearState();
    } catch(error) {
      let err = null;
      !error.message ? err = { message: error } : err = error;
      this.setState("errors", {...this.state.errors, cognito: err})
      console.log(this.state.errors.cognito.message);
    }
  }

}
